(($) => {
    `use strict`;
    $(document).ready(() => {
        $('.shopping-cart-popover, .click-no-close').on('click', (event) => {
            event.stopPropagation();
        });

        $('#sort-dropdown').selectize({
            // closeAfterSelect: true,
            onInitialize: function() {
                this.$control_input.attr('readonly', true);
            }
        });

        // initialize owl carousel
        $('.owl-carousel').owlCarousel({
            autoplay: true,
            mouseDrag: false,
            autoplayTimeout: 9000,
            responsiveClass: true,
            nav: false,
            margin: 24,
            loop: true,
            autoWidth: true,
            responsive: {
                0: {
                    items: 3,
                    center: true
                },
                992: {
                    items: 4,
                    margin: 10
                },
                1200: {
                    items: 4
                }
            }
        });
    });

})(jQuery);
